//
//  Extension.swift
//  DemoProject
//
//  Created by My Mac on 11/01/21.
//  Copyright © 2021 My Mac. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
import MobileCoreServices
//import GiFHUD_Swift


//import SHSnackBarView
import Alamofire
//import SVProgressHUD
import CoreLocation



func isInternetAvailable() -> Bool
{
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    
    var flags = SCNetworkReachabilityFlags()
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
        return false
    }
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    return (isReachable && !needsConnection)
}

func noInternetConnectionAlert(uiview: UIViewController) {
let alert = UIAlertController(title: "", message: "Please check your internet connection or try again later", preferredStyle: UIAlertController.Style.alert)
    if let popoverController = alert.popoverPresentationController {
           popoverController.sourceView = uiview.view //to set the source of your alert
                  popoverController.sourceRect = CGRect(x: uiview.view.bounds.midX, y: uiview.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
                  popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
              }
   alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
   uiview.present(alert, animated: true, completion: nil)
//
//    let alert = EMAlertController(title: ConstantVariables.Constants.Project_Name, message: "Please check your internet connection or try again later")
//    let action1 = EMAlertAction(title: "Ok", style: .cancel)
//    alert.addAction(action: action1)
//    uiview.present(alert, animated: true, completion: nil)
  }
