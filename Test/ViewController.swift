//
//  ViewController.swift
//  Test
///Users/mymac/Desktop/Test/Test.xcodeproj
//  Created by My Mac on 09/04/21.
//  Copyright © 2021 My Mac. All rights reserved.
//

import UIKit
import Alamofire

struct cellData {
   var opened = Bool()
   var title = String()
   var cardno = String()
   var sectionData = [String]()
   
    
}
struct cellData1 {
   var opened = Bool()
   var title = String()
  
   var sectionData1 = [String]()
    
}

class SavedCardcell: UITableViewCell
{
    
    @IBOutlet var uimg: UIImageView!
    @IBOutlet var lbltitle: UILabel!
}
class Otherpaymentoptions : UITableViewCell
{
    @IBOutlet var titlepaymentoptions: UILabel!
    
    @IBOutlet var imgTitle: UIImageView!
}
class SavedcardDetailcell : UITableViewCell
{
    @IBOutlet var lblrsmallround: UILabel!
    @IBOutlet var btnpay: UIButton!
    @IBOutlet var btn_select: UIButton!
    
    @IBOutlet var lblcardno: UILabel!
    
    @IBOutlet var txtccvv: UITextField!
    
    
}
class SamsungGpay : UITableViewCell
{
    @IBOutlet var btnpay: UIButton!
    
    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var lbltype: UILabel!
    
    @IBOutlet var lbldesc: UILabel!
  
    @IBOutlet var img: UIImageView!
    
}
class CreditCardCell : UITableViewCell
{
 
    
    @IBOutlet var lblcheckmark: UILabel!
    
    @IBOutlet var btnpay: UIButton!
}

class ViewController : UIViewController,UITableViewDelegate,UITableViewDataSource {

    
    var isSelected = 1
    
   var savedcard = [String]()
    
    @IBOutlet var tbloptionheight: NSLayoutConstraint!
    @IBOutlet var tblOtheroptions: UITableView!
    @IBOutlet var tblSavedCard: UITableView!
    
    var tableViewData = [cellData]()
    var tableViewData1 = [cellData1]()
    
    @IBOutlet var tblviewheight: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getRequestWithUrl(url: "http://ec2-13-233-32-142.ap-south-1.compute.amazonaws.com/api/merchant_pay_option.php", onCompletionHandler: {_ in })
           
        
        self.tblSavedCard.layoutIfNeeded()
        self.tblOtheroptions.layoutIfNeeded()
        
        tblSavedCard.layer.borderWidth = 1.0
        tblSavedCard.layer.cornerRadius = 10
        tblSavedCard.layer.borderColor = UIColor.lightGray.cgColor
        tblSavedCard.layer.masksToBounds = true
        
        tblOtheroptions.layer.borderWidth = 1.0
        tblOtheroptions.layer.cornerRadius = 10
        tblOtheroptions.layer.borderColor = UIColor.lightGray.cgColor
        tblOtheroptions.layer.masksToBounds = true
        
     
    }
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
      
        tblSavedCard.layoutIfNeeded()
        tblOtheroptions.layoutIfNeeded()
        
        self.tblviewheight?.constant = self.tblSavedCard.contentSize.height
        self.tbloptionheight?.constant = self.tblOtheroptions.contentSize.height
        
        
    }
    
     func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblSavedCard
        {
        return tableViewData.count
        }
        else
        {
            return tableViewData1.count
        }
        
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if tableView == tblSavedCard
         {
        if tableViewData[section].opened == true{
            return tableViewData[section].sectionData.count + 1
        } else{
            return 1
        }
        }
        else
         {
            if tableViewData1[section].opened == true{
                       return tableViewData1[section].sectionData1.count + 1
                   } else{
                       return 1
                   }
        }
    }
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblSavedCard
        {
        
        let dataIndex = indexPath.row - 1
        if indexPath.row == 0{
            guard  let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? SavedCardcell else {fatalError()}
            cell.lbltitle?.text = tableViewData[indexPath.section].title
            cell.uimg.image = UIImage(named:"cards")
            viewWillLayoutSubviews()
            return cell
            
        } else{
            
           guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as? SavedcardDetailcell else {fatalError()}
            
            cell.lblcardno?.text =  "XXXX XXXX XXXX " + tableViewData[indexPath.section].sectionData[dataIndex]
            
            cell.btn_select.layer.cornerRadius = 15
            cell.btn_select.layer.borderColor = UIColor.blue.cgColor
            cell.btn_select.layer.borderWidth = 1
            
            cell.lblrsmallround.layer.cornerRadius = 3
            cell.lblrsmallround.layer.borderColor = UIColor.blue.cgColor
            cell.lblrsmallround.layer.borderWidth = 1
            
            cell.btn_select.tag = indexPath.row
            cell.btn_select.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
            
            cell.btnpay.layer.cornerRadius = 5
            
            if indexPath.row == isSelected
            {
                cell.lblrsmallround.isHidden = false
                cell.txtccvv.isHidden = false
            }
            else
            {
                cell.lblrsmallround.isHidden = true
                cell.txtccvv.isHidden = true
            }
           
             viewWillLayoutSubviews()
            return cell
        }
        }
        else
        {
            let dataIndex = indexPath.row - 1
            if indexPath.row == 0{
               
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as? Otherpaymentoptions else {fatalError()}
                
           
                
                cell.titlepaymentoptions.text = tableViewData1[indexPath.section].title
                 if tableViewData1[indexPath.section].title == "Debit/Credit Card"
                 {
                    cell.imgTitle.image = UIImage(named:"cards")
                }
                else if tableViewData1[indexPath.section].title == "Samsung Pay"
                 {
                     cell.imgTitle.image = UIImage(named:"samsung")
                }
                else
                 {
                      cell.imgTitle.image = UIImage(named:"CUP")
                }
               viewWillLayoutSubviews()
                return cell
                
            } else{
                
                
                if tableViewData1[indexPath.section].title == "Debit/Credit Card"
                {
                    guard  let cell = tableView.dequeueReusableCell(withIdentifier: "cell3") as? CreditCardCell else {fatalError()}
                    
                   // cell.textLabel?.text = tableViewData1[indexPath.section].sectionData1[dataIndex]
                    cell.lblcheckmark.layer.cornerRadius = 4
                    cell.lblcheckmark.layer.borderColor = UIColor.lightGray.cgColor
                    cell.lblcheckmark.layer.borderWidth = 1
                    
                    cell.btnpay.layer.cornerRadius = 5
                     
                     viewWillLayoutSubviews()
                       return cell
                }
                else
                {
                  guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell4")  as? SamsungGpay else {fatalError()}
                   
                    print(tableViewData1[indexPath.section].sectionData1[dataIndex])
                    cell.lbltype.text = tableViewData1[indexPath.section].sectionData1[dataIndex]
                    
                   
                   if tableViewData1[indexPath.section].title == "Samsung Pay"
                     {
                         cell.img.image = UIImage(named:"samsung")
                        cell.lbldesc.text = "Use your Samsung Pay to make payment"
                    }
                    else
                     {
                          cell.img.image = UIImage(named:"CUP")
                         cell.lbldesc.text = "Use your China UnionPay to make payment"
                    }
                    
                         cell.btnpay.layer.cornerRadius = 5
                    return cell
                }
      
            }
            
        }
    }
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        if tableView == tblSavedCard
        {
        
        if indexPath.row == 0{
            if tableViewData[indexPath.section].opened == true {
                tableViewData[indexPath.section].opened = false
                let dataIndex = indexPath.row - 1
                if indexPath.row == 0{
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? SavedCardcell else {return}
                    cell.lbltitle?.text = tableViewData[0].title
                 
                    
                    
                } else{
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as? SavedcardDetailcell  else {return}
                    cell.textLabel?.text = tableViewData[indexPath.section].sectionData[dataIndex]
                    
                    
                }
                let sections = IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .none)
            } else{
                tableViewData[indexPath.section].opened = true
                let sections = IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .none)
            }
        }
        }
        else
        {
            if indexPath.row == 0{
                       if tableViewData1[indexPath.section].opened == true {
                           tableViewData1[indexPath.section].opened = false
                           let dataIndex = indexPath.row - 1
                           if indexPath.row == 0{
                               guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as? Otherpaymentoptions else {return}
                               cell.textLabel?.text = tableViewData1[indexPath.section].title
                               
                           } else{
                             guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell3") as? CreditCardCell else {return}
                               cell.textLabel?.text = tableViewData1[indexPath.section].sectionData1[dataIndex]
                           }
                           let sections = IndexSet.init(integer: indexPath.section)
                           tableView.reloadSections(sections, with: .none)
                       } else{
                           tableViewData1[indexPath.section].opened = true
                           let sections = IndexSet.init(integer: indexPath.section)
                           tableView.reloadSections(sections, with: .none)
                       }
                   }
        }
    }

    @objc func buttonAction(sender: UIButton) {
        print("Button pushed",sender.tag)
        isSelected = sender.tag
        self.tblSavedCard.reloadData()
        
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tblSavedCard
        {
        if indexPath.row == 0
        {
            return 54
            
        }
        else if isSelected == indexPath.row
        {
            return 137
            
            }
        else{
            return 88
            }
        }
        else{
            if indexPath.row == 0
            {
                      return 54
                      
                  }
                  else if tableViewData1[indexPath.section].title == "Debit/Credit Card" {
                return 230
                }
            else
            {
               return 154
            }
        }
         fatalError()
    }
    
    

        func getRequestWithUrl(url : String ,onCompletionHandler : @escaping ([String : Any]?) -> Void){

             
            if !isInternetAvailable(){
                          noInternetConnectionAlert(uiview: self)
                        
                       
                   
                    
            }
            
               else
            {
                Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
                    switch response.result {
                    case .success:
                        print(response)
                        
                     
                        
                        let data = response.result.value as! [String : Any]
                        
                        if let payoptions = data["payOptions"] as? [[String:Any]] {
                            payoptions.forEach { payoptions in
                                
                                let payOptDesc = payoptions["payOptDesc"] as? String ?? ""
                               self.tableViewData1.append(cellData1(opened: true, title: payOptDesc,sectionData1: [payOptDesc]))
                                
                                
                            }
                        }
                        if let savedCard = data["savedCard"] as? [[String:Any]] {
                                                   savedCard.forEach { SavedCards in
                                                       
                                                    let SavedCards = SavedCards["payCardNo"] as? String ?? ""
                                                    
                                                    self.savedcard.append(SavedCards)
                                                   
                                                       
                                                       
                                                   }
                            
                         
                            self.tableViewData.append(cellData(opened: true,title: "Saved Cards", sectionData:self.savedcard))
                            
                                               }

                      DispatchQueue.main.async {
                          self.tblSavedCard.reloadData()
                         self.tblOtheroptions.reloadData()
                        self.viewWillLayoutSubviews()
                      }
                       
                           
                        onCompletionHandler(response.result.value as? [String : Any])
                        break
                    case .failure(_):
                        onCompletionHandler(nil)
                    }
                    
                                        
                    
                }
        }
        
        }
    

}

